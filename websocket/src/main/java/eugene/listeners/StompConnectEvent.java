package eugene.listeners;

import eugene.entity.Chat;
import eugene.entity.ChatList;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;


public class StompConnectEvent implements ApplicationListener<SessionConnectEvent> {

  @Override
  public void onApplicationEvent(SessionConnectEvent sessionConnectEvent) {
    StompHeaderAccessor sha = StompHeaderAccessor.wrap(sessionConnectEvent.getMessage());

    Chat chat = new Chat();
    chat.setName(sha.getNativeHeader("chatName").get(0));
    chat.setSsesion(sha.getNativeHeader("sessionId").get(0));

    ChatList.addChat(chat);
  }
}
