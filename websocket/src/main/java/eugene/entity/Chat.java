package eugene.entity;

public class Chat {

  private String name;
  private String ssesion;

  public Chat() {
  }

  public Chat(String name, String ssesion) {
    this.name = name;
    this.ssesion = ssesion;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSsesion() {
    return ssesion;
  }

  public void setSsesion(String ssesion) {
    this.ssesion = ssesion;
  }
}
