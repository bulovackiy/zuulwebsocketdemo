package eugene.entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ChatList {

  private final static Map<String, HashSet<String>> chats = new HashMap<>();

  public static void addChat(Chat chat){
    if (!chats.containsKey(chat.getName()))
      chats.put(chat.getName(), new HashSet<>(Collections.singletonList(chat.getSsesion())));
    else
      chats.get(chat.getName()).add(chat.getSsesion());
  }

  public static HashSet<String> getSessionsOfChat(String chatName){
    return chats.get(chatName);
  }
}
