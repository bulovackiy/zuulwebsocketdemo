package eugene.controllers;

import eugene.entity.Chat;
import eugene.entity.ChatList;
import eugene.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class WebSocketController {

  private SimpMessagingTemplate template;

  @Autowired
  public WebSocketController(SimpMessagingTemplate template) {
    this.template = template;
  }


  @MessageMapping("/register/{chatName}/{sessionId}")
  public void registerChat(@DestinationVariable String chatName, @DestinationVariable String sessionId){
    Chat chat = new Chat(chatName, sessionId);
    ChatList.addChat(chat);
  }

  @MessageMapping("/send/message/{chatName}")
  public void onReceivedMessage(String message,
                                @DestinationVariable String chatName){

    Message msg = new Message(new SimpleDateFormat("HH:mm:ss").format(new Date())
      + " - " + message);

    ChatList.getSessionsOfChat(chatName).forEach(session ->
      this.template.convertAndSend(String.format("/chat/%1s/%2s", chatName, session), msg));
  }}
