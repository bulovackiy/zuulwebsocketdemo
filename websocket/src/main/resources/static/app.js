var stompClient;
var count = 0;

function connect(chatName){
    count++;
    var inputId = "input-{0}-{1}".format(chatName, count);
    var chatId = "{0}-{1}".format(chatName, count);
    var sessionId = Math.random().toString(36).replace('0.', '');

    $("#chat-container").append("<div>" +
                                    "<h1>{0}</h1>".format(chatName) +
                                    "<div id='{0}' class='chat'>".format(chatId) +
                                    "</div>" +
                                    "<label for='{0}'>Enter message: </label>".format(inputId) +
                                    "<input id='{0}' type='text'>".format(inputId) +
                                    "<button onclick=\"sendMessage(document.getElementById('{0}').value, '{1}', '{2}')\">Send</button>"
                                        .format(inputId, chatName, inputId) +
                                "</div>");
    var socket = new SockJS("/socket");
    stompClient = Stomp.over(socket);
    stompClient.connect({sessionId: sessionId, chatName: chatName}, function (frame) {
        stompClient.send("/app/register/{0}/{1}".format(chatName, sessionId), {}, {});
        console.log('Connected: ' + frame);
        stompClient.subscribe("/chat/{0}/{1}".format(chatName, sessionId), function (message) {
            $("#{0}".format(chatId)).append("<div class='message'>" + JSON.parse(message.body).content + "</div>");
            console.log(message.body);
        })
    });
}

function sendMessage(message, chatName, inputId) {
    this.stompClient.send("/app/send/message/{0}".format(chatName), {}, message);
    $("#{0}".format(inputId)).val('');
}

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}